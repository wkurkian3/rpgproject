import Physics

class StateMachine():
    def __init__(self,start,states,transitionFunction,actionFunction):
        self.states = states
        self.current = start
        self.transitionFunction = transitionFunction
        self.actionFunction = actionFunction
        
    def action(self,actor,area,time,delay):
        
        self.actionFunction(self.current,actor,area,time,delay)

    def transition(self,actor,area,time,delay):
        self.current = self.transitionFunction(self.current,actor,area,time,delay)

