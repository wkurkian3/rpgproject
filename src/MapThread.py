import threading
import Queue
import Physics
import Area

class MapThread(threading.Thread):
    
    def __init__(self,inqueue,outqueue):
        super(MapThread,self).__init__()
        self._stop =threading.Event()
        self.inqueue = inqueue
        self.outqueue = outqueue
        self.size = 50
        self.offsetTiles = None
    def run(self):
        while(self.isStopped() == False):
            job = self.inqueue.get()
            player = job["player"]
            area = job["area"]
            characters = area.actors
            area = Area.Area(Area.buildMapFile(area.name))
            area.setActors(characters)
            coordmap = {}
            tiles = area.tiles
            connections = area.connections
            links = []
            for i in connections.keys():
                links.append(i)
        #    connectionsPoints = 
            
            tileStart = area.getTileCoords(player.rect.topleft)
            if self.offsetTiles == None:    
                start = player.rect.topleft
                offset = [(start[0]-(self.size/2)*area.tilesize[0])*-1,(start[1]-(self.size/2)*area.tilesize[1])*-1]
                self.offsetTiles = [offset[1]/area.tilesize[1],offset[0]/area.tilesize[0]]
            count = 0
            #offsetTiles = [0,0]
            newArea = []
            check = {}
            newLoc = [int(self.size/2),int(self.size/2)]
            for y in range(self.size):
                newArea.append([])
                for x in range(self.size):
                    newArea[y].append(None)
            openList = []
            closed = []
            current = [list(tileStart),list(newLoc)]
            coordmap[self.toRaster(current[1])]     = tiles
            while count < self.size**2:
                coordsAround = [[current[0][0]-1,current[0][1]],[current[0][0]+1,current[0][1]],[current[0][0],current[0][1]-1],[current[0][0],current[0][1]+1]]
                newCoordsAround = [[current[1][0]-1,current[1][1]],[current[1][0]+1,current[1][1]],[current[1][0],current[1][1]-1],[current[1][0],current[1][1]+1]]
                currentArea = coordmap[self.toRaster(current[1])]
                for (x,y) in zip(coordsAround,newCoordsAround):
                    if  ( not self.coordsIn(current,closed)) and (Area.isTileIn(y,newArea) and newArea[y[0]][y[1]] == None):
                        link = False
                        areafile = ""
                        if coordmap[self.toRaster(current[1])] is tiles:
                            for i in links:
                                if x in connections[i][0]:
                                    #link = True
                                    areafile = connections[i][1]
                                    arealink = connections[i][2]
                        if link:
                            
                            coordmap[self.toRaster(y)] = Area.buildMapFile(areafile)[0]
                            x = arealink.transform(x)
                            
                        else:
                            coordmap[self.toRaster(y)] = coordmap[self.toRaster(current[1])]
                        if Area.isTileIn(x, coordmap[self.toRaster(y)]) and Area.isTileIn(y,newArea): 
                            openList.append([list(x),list(y)])
                        #print (newArea[y[0]][y[1]])
                
                if (not self.coordsIn(current,closed)) and Area.isTileIn(current[0], coordmap[self.toRaster(current[1])]) and Area.isTileIn(current[1],newArea) and newArea[current[1][0]][current[1][1]] == None:   
                    newArea[current[1][0]][current[1][1]] = currentArea[current[0][0]][current[0][1]].clone()
                    #print (newArea[current[1][0]][current[1][1]].rect.topleft)
                    #newArea[current[1][0]][current[1][1]].setTilePosition([current[1][0]-self.offsetTiles[0],current[1][1]-self.offsetTiles[1]])
                    #print (newArea[current[1][0]][current[1][1]].rect.topleft,current[1])
                    #print (current)
                    #check[self.toRaster(current[1])] = [newArea[current[1][0]][current[1][1]].rect.topleft,current[1],current[0]]
                count+=1
                if not newArea[current[1][0]][current[1][1]] == None:
                    closed.append(current)
                if not (len(openList) == 0):
                    current = openList.pop(0)
            #print ("done")
            midArea = [newArea,area.start,area.tilesize,area.connections,None,area.name]
            finalArea = Area.Area(midArea)
            finalArea.setActors(characters)
            self.outqueue.put(finalArea)
            sortedList = check.keys()
            sortedList.sort()
            #print (len(sortedList))
            #for i in sortedList:
                #print (i,check[i])
    def stop(self):
        self._stop.set()
            
    def isStopped(self):
        return self._stop.isSet()
    def toRaster(self, coords):
        return coords[0]*self.size+coords[1]
    def coordsIn(self,current,closed):
        for x in closed:
            if x[0] ==current[0]:
                return True
            elif x[1] == current[1]:
                return True
        return False