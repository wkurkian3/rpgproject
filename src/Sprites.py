import pygame
import Physics
import ImageProc
import copy
import Display
import item.MeleeWeapon as MeleeWeapon
import Character
import statemachine.MeleePursuit
import statemachine.RangedSkirmisher
import item.Item as ItemFactory
import Logger
import item.Consumable as Consumable
import random

#factory methods
def buildWarrior(character=Character.Character(),itemAttack =1, itemSpeed =1):
    monster = ImageProc.getSprite("graymonster",[3,4])
    enemy = Enemy(monster["images"],character,monster["properties"],statemachine.MeleePursuit.MeleePursuit())
    item = ItemFactory.buildSword(itemAttack,itemSpeed)
    enemy.item = item
    return enemy

def buildArcher(character=Character.Character(),itemAttack=1,itemSpeed=1):
    soldier = ImageProc.getSprite("soldier",[3,4])
    enemy = Enemy(soldier["images"],character,soldier["properties"],statemachine.RangedSkirmisher.RangedSkirmisher())
    item = ItemFactory.buildBow()
    enemy.item = item
    return enemy


class Rectangle(pygame.sprite.Sprite):
    def __init__(self, initialPosition,size, color):

        pygame.sprite.Sprite.__init__(self)
        
        self.image = pygame.Surface([size, size])
        self.image.fill(color)
        self.color = color

        self.rect = self.image.get_rect()
        self.rect.topleft = initialPosition
        self.nextUpdate = 0

    def update(self,time):
        
        if self.nextUpdate < time:
            temp = [-255,-255,-255]
class Person(pygame.sprite.Sprite):
    def __init__(self,images,character, properties,inputP = None):

        pygame.sprite.Sprite.__init__(self)

        if inputP == None:
            self.ai = True
            self.input = []
        else:
            self.ai = False
            self.input = inputP
        

        self.character = character
        self.item = None
        self.itemActive = False
        self.itemFrame = [[0,0],None]
        self.itemArea = None
        self.itemUpdate = 0
        self.time = 0
        self.image = images[0][1]
        self.images = images
        self.currentImageIndex = 0
        self.currentAnimationIndex = 1
        self.rect = self.image.get_rect()
        #self.rect.topleft = initialPosition
        self.nextUpdate = 0
        self.props = properties
        self._layer = 1
        self.layerChanged = 0
        self.moved = False;
        self.path = []
        self.current = [0,0]
        
        self.handPosition = [0,0]
        
        self.pickupWait = False
        self.pickupTimer = 0

        self.sideItem = None
        self.sideItemActive = False
        self.sideItemUpdate = 0
        self.sideItemFrame = None
    def mainAction(self,area):
        if not self.item == None and self.itemActive == False:
            
            self.itemActive = True
            
            self.itemArea = area
            if not self.ai:
                Logger.addItemUse(self.item.type)
            
    def sideAction(self,area):
        if not self.sideItem == None and self.sideItemActive == False:
            
            self.sideItemActive = True
            
            self.itemArea = area
            if not self.ai:
                Logger.addItemUse(self.sideItem.type)
        
    def update(self,time,area):
        self.handPosition = Physics.vectorAdd(self.rect.topleft,self.props.getArray('handPosition'))
        
        temp = self.rect
        layer = self.rect.midbottom[1]
        #print self._layer'
        if layer != self._layer:
            self._layer = layer
            self.layerChanged = 1

        if self.pickupWait and self.pickupTimer == 0:
            self.pickupTimer = time+300
        elif self.pickupWait and self.pickupTimer < time:
            self.pickupWait = False
            self.pickupTimer = 0

        if self.itemActive and self.itemUpdate < time:
            effect = self.item.doEffect(self,self.itemArea)
            self.itemActive = effect[0]
            self.itemUpdate = time+effect[3]
            if self.itemActive:
                self.itemFrame = [effect[1],effect[2]]
            else:
                self.itemFrame = [[0,0],None]
        if self.sideItemActive and self.sideItemUpdate < time:
            effect = self.sideItem.doEffect(self,self.itemArea)
            self.sideItemActive = effect[0]
            self.sideItemUpdate = time+effect[3]
            if self.sideItemActive:
                self.sideItemFrame = [effect[1],effect[2]]
            else:
                self.sideItemFrame = [[0,0],None]
                
        if self.nextUpdate < time and self.moved:
            self.nextUpdate = time+100
            self.currentAnimationIndex +=1
            if (self.currentAnimationIndex > 2):
                self.currentAnimationIndex = 0
            self.image = self.images[self.currentImageIndex][self.currentAnimationIndex]
            self.moved =False
    def getHandPosition(self):
        return self.handPosition
    def damage(self,damage,area=None):
        self.character.health -= max(damage-self.character.defense,0)
        if not self.ai:
            Logger.damageTaken(damage)
        if self.character.health <= 0:
            if not area==None:
                #do default character dead behavior
                area.removeActor(self)
                Logger.enemyKilled()
                #self.drop(self.item,area)
            return True
        else:
            return False
    def drop(self,item,area):
        drop = Item(item.getIcon(),item)
        drop.rect.topleft = self.rect.center
        area.setItems(area.items + [drop])
        passive = getattr(self, "removePassiveEffect", None)
        if callable(passive):
            passive(self)
    def pickup(self,area):
        if self.pickupWait:
            return
        item = None
        for i in area.items:
            if i.rect.colliderect(self.rect):
                item = i
        oldItem = self.item
        oldSideItem = self.sideItem
        if not item == None and not item.item.type==ItemFactory.Item.CONSUMABLE:
            if item.item.type == ItemFactory.Item.OTHER:
                self.sideItem = item.item
                self.sideItemActive = False
                area.removeItem(item)
                if not oldSideItem == None:
                    self.drop(oldSideItem,area)
                passive = getattr(item.item, "doPassiveEffect", None)
                if callable(passive):
                    passive(self)
            else:
                self.item = item.item
                self.itemActive = False
                area.removeItem(item)
                if not oldItem == None:
                    self.drop(oldItem,area)
        elif not item == None:
            item.item.pickupEffect(self,area)
            area.removeItem(item)
        self.pickupWait = True
                
    def move(self,direction,step=5):
        if self.sideItemActive == True:
            return
        self.moved = True
        self.rect.midbottom = Physics.vectorAdd(self.rect.midbottom,Physics.vectorByScalar(direction,step))
        dir = [0,0]
        if direction[0] != 0:
            dir[0] = direction[0]/abs(direction[0])
        if direction[1] != 0:
            dir[1] = direction[1]/abs(direction[1])
        if dir[0] != 0 and dir[1] != 0:
            dir[1] = 0
        self.changeDirection(dir)
    def changeDirection(self,direction):
        self.currentImageIndex = ImageProc.getIndexFromDir(self.images,direction)
        self.image = self.images[self.currentImageIndex][self.currentAnimationIndex]    
    def setPosition(self,position):
        self.rect.topleft = position
    def clone(self):
        person = Person(self.images,self.character.clone(),copy.deepcopy(self.props),self.input)
        person.rect.topleft = self.rect.topleft
        return person
    def followPath(self):
        if (len(self.path) > 0):
                self.walkMax = 6
                current = self.path.pop()
                move = [0,0]
                count =0
                while count < self.walkMax:
                    #print (count,current,len(self.path),move)
                    if current == [0,0] and len(self.path) == 0:
                        break
                    while current == [0,0] and len(self.path) > 0:
                        current = self.path.pop()
                
                    current[0],c = self.approachZero(current[0])
                    move[0] += c
                    if c != 0:
                        count += 1
                    current[1],c = self.approachZero(current[1])
                    move[1] += c
                    if c != 0:
                        count += 1
                if current != [0,0]:
                    #print (current)
                    self.path.append(current)
                self.move(move,1)
        
    def decide(self,area,time, keys,mouse):
        if time - self.time > 100-min(self.character.speed**0.5,30)*2.5:
            self.time  = time
        else:
            return -1
        
        
        self.followPath()
                
        if not self.ai:
            path =[]
            for key in self.input:
                if (keys[key[0]]):
                    key[1][0](*([self,area] + key[1][1]))
                    
            midpos = [0,0]
            walkbox = self.props.getWalkbox(self.image)
            if mouse[0]:
                
                midpos = [mouse[1][0]-walkbox[2]*.5,mouse[1][1]-walkbox[3]]
            # midpos = [mouse[1][0]-self.rect.width*.5,mouse[1][1]-self.rect.height]
            #if (mouse[0]):
                #print(area.testPassable(self.clone(),mouse[1]))
            if mouse[0] and area.testPassable(self.clone(),mouse[1]):
                targetPos =mouse[1]
                path = area.getPath(self.clone(),self.rect.midbottom,targetPos)
                self.path = path
        return 0
            
            
#self.rect.midbottom = Physics.vectorAdd(current,self.rect.midbottom)

    def approachZero(self,number):
        ret = 0
        if number < 0:
            number+= 1
            ret = -1
        elif number > 0:
            number -= 1
            ret = 1
        return number,ret
            
        
class Enemy(Person):
    def __init__(self,images,character, properties, machine=None):
        Person.__init__(self,images,character,properties,None)
        self.machine = machine
        self.data = {}
    def update(self,time,area):
        Person.update(self,time,area)
    def damage(self,damage,area=None):
        self.character.health -= damage
        if not self.ai:
            Logger.damageTaken(damage)
        if self.character.health <= 0:
            if not area==None:
                #do default character dead behavior
                area.removeActor(self)
                Logger.enemyKilled()
                if random.random() <= 0.5:
                    type = random.randint(0,Consumable.NUMSTATS)
                    powerup = ItemFactory.buildStatUp(type,random.randint(1,2))
                    self.drop(powerup,area)
            return True
        else:
            return False
    
    def clone(self):
        enemy = Enemy(self.images,self.character.clone(),copy.deepcopy(self.props),self.input)
        enemy.rect.topleft = self.rect.topleft
        return enemy
    def decide(self,area,time, keys,mouse):
        cont = Person.decide(self,area,time,keys,mouse)
        #if self.item.type == ItemFactory.Item.RANGED:
            #print self.data
        if (cont == -1):
            return
        if not self.machine == None:
            self.machine.transition(self,area,time,self.data)
            self.machine.action(self,area,time,self.data)
        
class Tile(pygame.sprite.Sprite):
    def __init__(self, initialPosition,image,properties,_layer):

        pygame.sprite.Sprite.__init__(self)

        self.image = image
        self.rect = self.image.get_rect()
        self.rect.topleft = initialPosition
        self.nextUpdate = 0
        self.props = properties
        self._layer =_layer
        self.layerChanged = 0
        self.zone = 1
    def update(self,time,area):
        if self.nextUpdate < time:
            pass
    
    def move(self,direction,step=5):
        self.rect.topleft += Physics.vectorByScalar(direction,step)
    def setTilePosition(self, tileCoords):
        self.rect.topleft =  [tileCoords[1]*self.image.get_width(),tileCoords[0]*self.image.get_height()]
    def setPosition(self,position):
        self.rect.topleft = position
    def clone(self):
        return Tile(list(self.rect.topleft),self.image,copy.deepcopy(self.props),self._layer)
class Item(pygame.sprite.Sprite):
    def __init__(self, image,item):

        pygame.sprite.Sprite.__init__(self)

        self.image = image
        self.rect = self.image.get_rect()
        self.nextUpdate = 0
        self._layer = 1
        self.layerChanged = 0
        self.item = item
    def update(self,time,area):
        if self.nextUpdate < time:
            layer = self.rect[1]
            if layer != self._layer:
                self._layer = layer
                self.layerChanged = 1
            self.nextUpdate += time+100
        
    def clone(self):
        return Item(list(self.image,item))
    
