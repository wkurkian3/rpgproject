
class Item:
    MELEE = 1
    RANGED = 2
    MAGIC = 3
    OTHER = 4
    CONSUMABLE = 5
    type = -1 
    def __init__(self):
        x = 0
    def doEffect(self,area):
        animation = None
        return [False,animation]


import ImageProc
import Sprites
import Spell
import MeleeWeapon
import Ranged
import Consumable
import Utility

def buildSword(attack = 1,speed = 1):
    swords = ['Slash_left','Slash_up','Slash_left','Slash_up']
    item = MeleeWeapon.Sword(attack,speed,swords)
    return item
def buildSwordPickup(attack=1,speed=1):
    swords = ['Slash_left','Slash_up','Slash_left','Slash_up']
    item = buildSword(attack,speed)
    icon = ImageProc.getIcon('sword')
    pickup = Sprites.Item(icon,item)
    return pickup

def buildFrostSpell(attack=1,speed=1):
    item = Spell.FrostSpell(attack,speed,'frost')
    return item
def buildFrostSpellPickup(attack=1,speed=1):
    item = buildFrostSpell(attack,speed)
    icon = item.getIcon()
    pickup = Sprites.Item(icon,item)
    return pickup

def buildBow(attack=1,speed=1):
    item = Ranged.Bow(attack,speed)
    return item
def buildBowPickup(attack=1,speed=1):
    item = buildBow(attack,speed)
    icon = item.getIcon()
    pickup = Sprites.Item(icon,item)
    return pickup

def buildGrapplingHook(speed= 0):
    item = Utility.GrapplingHook(speed)
    return item
def buildGrapplingHookPickup(speed = 0):
    item = buildGrapplingHook(speed)
    icon = item.getIcon()
    pickup = Sprites.Item(icon,item)
    return pickup

def buildShield(defense=2):
    item = Utility.Shield(defense)
    return item
def buildShieldPickup(defense = 2):
    item =  buildShield(defense)
    icon = item.getIcon()
    pickup = Sprites.Item(icon,item)
    return pickup

def buildStatUp(stat=Consumable.ATTACK,magnitude=1):
    item = Consumable.StatUp(stat,magnitude)
    return item
def buildStatUpPickup(stat=Consumable.ATTACK,magnitude=1):
    item = buildStatUp(stat,magnitude)
    icon = item.getIcon()
    pickup = Sprites.Item(icon,item)
    return pickup
