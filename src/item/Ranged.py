import Item
import ImageProc
import Physics
import Display
import pygame
import pygame.transform

class Bow(Item.Item):
    attack = 0
    speed = 0
    images = []
    frame = 0
    def __init__(self,attack,speed,images=None):
        self.type = Item.Item.RANGED
        self.attack = attack
        self.speed = speed
        self.time = 20-speed
        
        
        tempImages = ImageProc.getAnimation('bow')
        tempImages2 = ImageProc.getAnimation('arrow')
        self.size = [tempImages['size'],tempImages2['size']]

        self.images = tempImages['images'] + tempImages2['images']

        
        self.props = [tempImages['props'],tempImages2['props']]
        self.hit = []
        self.travel = 5
        self.count = 0
        self.pos = None
        self.hitbox = None
    def getIcon(self,name =None):
        if name == None:
            name = 'bow'
        return ImageProc.getIcon(name)
    def doEffect(self,source,area):

        if self.pos == None:
            self.pos = source.getHandPosition()
            self.hitbox = source.rect.midbottom
            center = [int(self.size[0][0]*0.4),int(self.size[0][1]*0.5)]
            hitcenter = [int(self.size[0][0]*0.4),int(self.size[0][1]*0.35)]
            self.pos = Physics.vectorSub(self.pos,center)
            self.hitbox = Physics.vectorSub(self.hitbox,hitcenter)
            ind =source.currentImageIndex
            if ind == 0:
                self.rotation = 90
                self.direction = [0,-1]
            elif ind == 1:
                self.rotation = 0
                self.direction = [1,0]
            elif ind == 2:
                self.rotation = -90
                self.direction = [0,1]
            elif ind == 3:
                self.rotation = -180
                self.direction = [-1,0]
        if self.frame >0:
            distance = self.travel
            offset = Physics.vectorByScalar(self.direction,distance)
            self.pos = Physics.vectorAdd(self.pos,offset)  
            self.hitbox = Physics.vectorAdd(self.hitbox,offset)
        
        hitbox = self.props[1].getArray('hitbox')
        hitbox[0] += self.hitbox[0]
        hitbox[1] += self.hitbox[1]
        circleBox = None
        if self.rotation == 0 or self.rotation == -180: 
            circleBox = pygame.Rect(hitbox[0],hitbox[1],hitbox[2],hitbox[3])
        else:
            circleBox = pygame.Rect(hitbox[0],hitbox[1],hitbox[3],hitbox[2])
        actors = area.actors
        boxes = [a.rect for a in actors]
        if self.frame > 0:
            
            indices = circleBox.collidelistall(boxes)
            self.damaged = []
            if len(indices) > 0:
                  for i in indices:
                        actor = actors[i]
                        if not actor is source and not actor in self.hit:
                              self.damaged = self.damaged + [actor]
            
            for actor in self.damaged:
                  if not actor in self.hit:
                        dead = actor.damage(self.attack+source.character.attack,area)
                        self.hit = self.hit + [actor]
        if len(self.hit) > 0:
            self.stop()
            return [False,[self.pos],[],500]
        elif self.frame == 0:
            image = pygame.transform.rotate(self.images[self.frame],self.rotation)
            self.frame = self.frame+1
            return [True,[self.pos],[image],self.time]
        elif not area.checkWall(circleBox):
            
            self.stop()
            return [False,[self.pos],[None],500]
        else:
            image = pygame.transform.rotate(self.images[self.frame],self.rotation)
            self.count += 1
            more = True if self.count <= 30 else False
            time = self.time
            if not more:
                self.stop()
                time = 500
            return [more,[self.pos],[image],time]
        
    def stop(self):
        self.frame = 0
        self.hit = []
        self.travel = 5
        self.pos = None
        self.hitbox = None
        self.count = 0
