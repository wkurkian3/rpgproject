import Item
import ImageProc
import pygame.transform
import Physics
import Display
import pygame

ATTACK = 0
SPEED = 1
HEALTH = 2
NUMSTATS = 3

class StatUp(Item.Item):
      frame = 0
      attack = 0
      speed = 0
      images = []
      def __init__(self,stat,magnitude):
            self.type = Item.Item.CONSUMABLE
            self.stat = stat
            self.magnitude = magnitude
            self.time = 0
            
      def getIcon(self, name=None):
            if name ==None:
                name = 'stat'
            if self.stat == ATTACK:
                name = 'stat_attack'
            elif self.stat == SPEED:
                name = 'stat_speed'
            elif self.stat == HEALTH:
                name = 'stat_health'
            return ImageProc.getIcon(name)
      def pickupEffect(self,source,area):
          if self.stat == ATTACK:
              source.character.attack += self.magnitude
          elif self.stat == SPEED:
              source.character.speed += self.magnitude
          elif self.stat == HEALTH:
              source.character.health += self.magnitude
      def doEffect(self,source,area):
            pass
