import Item
import ImageProc
import pygame.transform
import Physics
import Display
import pygame

class Sword(Item.Item):
      frame = 0
      attack = 0
      speed = 0
      images = []
      def __init__(self,attack,speed,animation):
            self.type = Item.Item.MELEE
            self.attack = attack
            self.speed = speed
            self.time = 70
            
            tempImages =  [[],[],[],[]]
            tempImages[0] = ImageProc.getAnimation(animation[0])
            tempImages[1] = ImageProc.getAnimation(animation[1])
            tempImages[2] = ImageProc.getAnimation(animation[2])
            tempImages[3] = ImageProc.getAnimation(animation[3])

            self.size = [[],[],[],[]]
            self.size[0] = tempImages[0]['size']
            self.size[1] = tempImages[1]['size']
            self.size[2] = tempImages[2]['size']
            self.size[3] = tempImages[3]['size']

            images =  [[],[],[],[]]
            images[0] = tempImages[0]['images']
            images[1] = tempImages[1]['images']
            for i in range(len(images[1])):
                  images[1][i] = pygame.transform.flip(images[1][i],True,False)
            images[2] = tempImages[2]['images']
            for i in range(len(images[2])):
                  images[2][i] = pygame.transform.flip(images[2][i],False,True)
            images[3] = tempImages[3]['images']
            self.images = images
            
            self.props = [[],[],[],[]]
            self.props[0] = tempImages[0]['props']
            self.props[1] = tempImages[1]['props']
            self.props[2] = tempImages[2]['props']
            self.props[3] = tempImages[3]['props']

            self.hit = []
      def getIcon(self, name=None):
            if name ==None:
                  name = 'sword'
            return ImageProc.getIcon(name)
      def doEffect(self,source,area):
            pos =source.getHandPosition()
            offset = self.props[source.currentImageIndex].getArray('handleOffset')
            if  source.currentImageIndex == 1 or source.currentImageIndex == 2:
                  offset = Physics.vectorSub(self.size[source.currentImageIndex],offset)
            pos = Physics.vectorSub(pos,offset)
            images = []
            positions = []
            ind =source.currentImageIndex
            
            if self.frame < len(self.images[ind]): 
                  boxPos = [0,0,0,0]
                  if ind == 0:
                        boxPos = self.props[ind].getArray('hitbox'+str(self.frame))
                  elif ind == 1:
                        boxPos = self.props[ind].getArray('hitbox'+str(self.frame))
                        offset = [20-boxPos[2],0,0,0]
                        boxPos = Physics.vectorSub(boxPos,offset)
                        
                  elif ind == 2:
                        boxPos = self.props[ind].getArray('hitbox'+str(self.frame))
                        offset = [0,24-boxPos[3],0,0]
                        boxPos = Physics.vectorSub(boxPos,offset)
                        
                  elif ind == 3:
                        boxPos = self.props[ind].getArray('hitbox'+str(self.frame))
            #collision behavior
                        
                  boxPos[0:2] = Physics.vectorAdd(pos,boxPos)
                  
                  swordBox = pygame.Rect(boxPos[0],boxPos[1],boxPos[2],boxPos[3])
                  
                  #if not swordBox == None:
                  #      collide = pygame.Surface((swordBox.width,swordBox.height))
                 #       collide.fill([255,255,255])
                  #      images.append(collide)
                  #      positions.append(swordBox.topleft)
                  actors = area.actors
                  boxes = [a.rect for a in actors]
                  indices = swordBox.collidelistall(boxes)
                  self.damaged = []
                  if len(indices) > 0:
                        for i in indices:
                              actor = actors[i]
                              if not actor is source and not actor in self.hit:
                                    self.damaged = self.damaged + [actor]
            
                  for actor in self.damaged:
                        if not actor in self.hit:
                              dead = actor.damage(self.attack+source.character.attack,area)
                              self.hit = self.hit + [actor]
            positions.insert(0,pos)
            if self.frame == 0:
                  image = self.images[source.currentImageIndex][self.frame]
                  self.frame = self.frame +1
                  images.insert(0,image)
                  
                  return [True,positions,images,self.time]
            else:
                  more = True if self.frame < len(self.images[source.currentImageIndex]) else False
                  
                  image = None
                  if more:

                        image = self.images[source.currentImageIndex][self.frame]
                        self.frame = self.frame + 1
                  else:
                        self.frame = 0
                        self.hit = []
                  images.insert(0,image)
                  
                  return [more,positions,images,self.time]
