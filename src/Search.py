import Physics
import Area
import Display
import Input
import math

class Node:
    def __init__(self,pos,prev,prior,st):
        self.position = pos
        self.previous = prev 
        self.priority = prior
        self.steps = st

def heuristic(node,start):
    return int(math.sqrt((node[0]-start[0])**2+(node[1]-start[1])**2))

def key(node):
    return node.steps+node.priority

def findTilePath(area,actor,currentPos, targetPos,limit = -1):
    count = 0
#print Area.isTileIn(area.getTileCoords(targetPos),area.tiles)
    #mainOffset = [actor.rect.width*.5,actor.rect.height]
    startPos = currentPos #Physics.vectorSub(currentPos,mainOffset)
    startTile = area.getTileCoords(startPos)
    startTilePos = area.getTilePosition(startTile)
    targetTile = area.getTileCoords(targetPos)
    #print targetTile
    #print final, startTilePos,mainOffset,currentPos
    open = [Node(startPos,None,0,0)]
    closed = []
    dots = []
    while (len(open) > 0 ):
        count += 1
        currentNode = open.pop(0)
        if limit > 0:
            if count == limit*12:
                return []
        #dots.append(currentNode.position)
        current = currentNode.position
        #print ("node:", current)
        coordsAround = [[current[0]-5,current[1]],[current[0]+5,current[1]],[current[0],current[1]-5],[current[0],current[1]+5]]
        coordsAround += [[current[0]-5,current[1]+5],[current[0]+5,current[1]+5],[current[0]-5,current[1]-5],[current[0]+5,current[1]-5]]
        
        test = Physics.vectorabs(Physics.vectorSub(current,targetPos))
        
        if (test[0] < 6 and test[1] < 6):
            break
        for node in coordsAround:
            #print (Area.isTileIn(area.getTileCoords(node), area.tiles),area.testPassable(actor,node))
            if Area.isTileIn(area.getTileCoords(node), area.tiles) and not (node in closed) and area.testPassable(actor,node):
                priority = heuristic(node,targetPos)
                n = Node(node,currentNode,priority,currentNode.steps+1)
                open.append(n)
                closed.append(node)
        open = sorted(open,key=key)
    #print (targetPos, currentNode.position, startPos)
    path = []
    endPos = currentNode.position
    #print "start"
    test = Physics.vectorabs(Physics.vectorSub(current,targetPos))
    if not(test[0] < 6 and test[1] < 6):
        return path
    while (currentNode.previous != None):
        #print currentNode.tilePosition,targetTile,startTile
        previous = currentNode.previous.position
        currentPos = currentNode.position
        #previous = Physics.vectorAdd(previous, mainOffset)
        #currentPos = Physics.vectorAdd(currentPos, mainOffset)
        offset = Physics.vectorSub(currentPos,previous)
        path.append(offset)
        currentNode = currentNode.previous
        Display.setDots(dots)
    #path.reverse()
    
    return path
