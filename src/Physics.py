def vectorAdd(vec1,vec2):
    l1 = len(vec1)
    l2 = len(vec2)
    items = min(l1,l2)
    new = []
    for i in range(items):
        new.append(vec1[i]+vec2[i])

    return new
def vectorSub(vec1,vec2):
    l1 = len(vec1)
    l2 = len(vec2)
    items = min(l1,l2)
    new = []
    for i in range(items):
        new.append(vec1[i]-vec2[i])

    return new

def vectorByScalar(vec,scalar):
    new = []
    for i in range(len(vec)):
        new.append(vec[i] * scalar)
    return new

def setVectorValue(vec,value):
    for i in range(len(vec)):
        vec[i] = value

    return vec

def vectorabs(vec):
    ret = []
    for i in vec:
        ret.append(abs(i))
    return ret
#    0
# 3      1
#    2
#
def directionToCardinalNumber(direction):
    ret = -1
    if (direction[0] == 0 and direction[1] == -1):
        ret = 0
    elif (direction[0] == 1 and direction[1] == 0):
        ret = 1
    elif (direction[0] == 0 and direction[1] == 1):
        ret = 2
    elif (direction[0] == -1 and direction[1] == 0):
        ret = 3
    else:
        ret = 0
    return ret
def cardinalNumberToDirection(number):
    ret = [0,0]
    if (number == 0):
        ret = [0,-1]
    elif (number == 1):
        ret = [1,0]
    elif (number == 2):
        ret = [0,1]
    elif (number == 3):
        ret = [-1,0]
    else:
        ret = [1,0]
    return ret
def RectContains(rect, point):
    xtest = point[0]-rect[0]
    if (xtest >= 0 and xtest <= rect[2]):
        ytest = point[1]-rect[1]
        if (ytest >= 0 and ytest <= rect[3]):
            return True
    return False

def normalizeVector(vec):
    ret = []
    for i in vec:
        if i > 0:
            ret.append(1)
        elif i == 0:
            ret.append(0)
        else:
            ret.append(-1)
            
    return ret
    
