from level.agent.agent import Agent
import random

class DiggingAgent(Agent):
    NORTH = 1
    EAST = 2
    SOUTH = 3
    WEST = 4

    def __init__(self, startx, starty, endx, endy, area, width, height, player_model):
        super(DiggingAgent, self).__init__(area, width, height, player_model)
        self.agents = []
        self.startx = startx
        self.starty = starty
        self.endx = endx
        self.endy = endy
        if (endx - startx > 20 and endy - starty > 20):
            self.endx = (startx + endx) / 2
            self.endy = (starty + endy) / 2
            self.agents.append(DiggingAgent((startx + endx) / 2, starty, endx, (starty + endy) / 2, area, width, height, player_model))
            self.agents.append(DiggingAgent(startx, (starty + endy) / 2, (startx + endx) / 2, endy, area, width, height, player_model))
            self.agents.append(DiggingAgent((startx + endx) / 2, (starty + endy) / 2, endx, endy, area, width, height, player_model))
        self.coords = [(self.startx + self.endx) / 2, (self.starty + self.endy) / 2]

    def execute(self):
        for agent in self.agents:
            agent.execute()

        changeDirBase = 0.15 + 0.1 * self.player_model.meleePref - 0.1 * self.player_model.rangedPref
        changeDir = changeDirBase
        placeRoomBase = 0.15 + 0.1 * self.player_model.rangedPref - 0.1 * self.player_model.meleePref
        placeRoom = 1
        direction = self.randomDirection()

        for i in range(100):
            self.move_dig(direction)
            self.dig()
            if (random.random() < changeDir):
                direction = self.randomDirection()
                changeDir = 0
            else:
                changeDir += changeDirBase
            if (random.random() < placeRoom):
                roomWidth = random.randint(3, 5)
                roomHeight = random.randint(3, 5)
                self.digRoom(roomWidth, roomHeight)
                placeRoom = 0
            else:
                placeRoom += placeRoomBase

    def randomDirection(self):
        return random.randint(1,4)

    def dig(self):
        self.area[self.coords[1]][self.coords[0]] = 3

    def digRoom(self, roomWidth, roomHeight):
        for y in range(roomHeight):
            for x in range(roomWidth):
                w =  self.coords[0] - x
                h =  self.coords[0] - y
                if (w < 1):
                    w = 1
                if (h < 1):
                    h = 1
                if (w >= self.width - 1):
                    w = self.width - 2
                if (h >= self.height - 1):
                    h = self.height - 2
                self.area[h][w] = 3

    def move_dig(self, direction):
        if (direction == self.NORTH):
            self.coords[1] -= 1
        elif (direction == self.SOUTH):
            self.coords[1] += 1
        elif (direction == self.WEST):
            self.coords[0] -= 1
        elif (direction == self.EAST):
            self.coords[0] += 1
        if (self.coords[1] < 1):
            self.coords[1] = 1
        if (self.coords[0] < 1):
            self.coords[0] = 1
        if (self.coords[0] == self.width - 1):
            self.coords[0] = self.width - 2
        if (self.coords[1] == self.height - 1):
            self.coords[1] = self.height - 2
    
